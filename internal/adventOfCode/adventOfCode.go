package adventOfCode

import (
    "io/ioutil"
	"regexp"
	"errors"
	"math/big"
)

func Slurp( file string ) ( []string, error ) {
	b, err := ioutil.ReadFile( file )
	if err != nil {
		return nil, err
	}

	newline := regexp.MustCompile( "[\r\n]+" )

	content := newline.Split( string(b), -1 )

	if len( content ) > 0 && content[ len(content) - 1 ] == "" {
		content = content[:len(content) - 1]
	}

	return content, nil
}

func Min( nums ...int ) ( int, error ) {
	if len( nums ) == 0 {
		return 0, errors.New( "Empty list" )
	}

	min := nums[0]

	for i := 1; i < len( nums ); i++ {
		if min > nums[i] {
			min = nums[i]
		}
	}

	return min, nil
}

func Max( nums ...int ) ( int, error ) {
	if len( nums ) == 0 {
		return 0, errors.New( "Empty list" )
	}

	max := nums[0]

	for i := 1; i < len( nums ); i++ {
		if max < nums[i] {
			max = nums[i]
		}
	}

	return max, nil
}

func MinRune( nums ...rune ) ( rune, error ) {
	if len( nums ) == 0 {
		return 0, errors.New( "Empty list" )
	}

	min := nums[0]

	for i := 1; i < len( nums ); i++ {
		if min > nums[i] {
			min = nums[i]
		}
	}

	return min, nil
}

func MaxRune( nums ...rune ) ( rune, error ) {
	if len( nums ) == 0 {
		return 0, errors.New( "Empty list" )
	}

	max := nums[0]

	for i := 1; i < len( nums ); i++ {
		if max < nums[i] {
			max = nums[i]
		}
	}

	return max, nil
}


func Crt(mod []int, rem []int ) ( int, error ) {
	ms := []*big.Int{}
	as := []*big.Int{}
	for i:=0; i<len(mod); i++ {
		if mod[i] == 0 {
			return 0, errors.New("modulos of 0 found")
		}
		ms = append(ms, big.NewInt(int64(mod[i])) )
		as = append(as, big.NewInt(int64(rem[i])) )
	}

	m := big.NewInt(1)
	for i := 0; i < len(ms); i++ {
		m = m.Mul(m, ms[i])
	}

	x := big.NewInt(0)
	for i := 0; i < len(ms); i++ {
		mi := new(big.Int).Div(m, ms[i])
		ti := new(big.Int).ModInverse(mi, ms[i])

		s := new(big.Int).Mul(as[i], ti)
		s = s.Mul(s, mi)
		x = x.Add(x, s)
		x = x.Mod(x, m)
	}

	return int(x.Int64()), nil
}

