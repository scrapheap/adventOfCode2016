GO=go

.PHONY: all
all: test build

.PHONY: test
test:
	$(GO) test -v ./...

bin/%: cmd/%/main.go
	$(GO) build -o $@ -v $^
