package main

import(
    "fmt"
	"os"
	"strconv"
	"container/list"
	"adventOfCode/internal/adventOfCode"
)

type elf struct {
	id int
	presents int
}

func Part1( input []string ) int {
	numOfElves, err := strconv.Atoi( input[0] )
	if err != nil {
		panic( err )
	}

	elves := []elf{}

	for i:=1; i <= numOfElves; i++ {
		elves = append( elves, elf{ id: i, presents: 1 } )
	}

	for len( elves ) > 1 {
		elves[0].presents += elves[1].presents;
		elves = append( elves[2:], elves[0] )
	}

	return elves[0].id
}

func Part2( input []string ) int {
	numOfElves, err := strconv.Atoi( input[0] )
	if err != nil {
		panic( err )
	}

	elves := list.New()

	for i:=1; i <= numOfElves; i++ {
		elves.PushBack( elf{ id: i, presents: 1 } )
	}

	cursor:= elves.Front()
	idx := 0

	for elves.Len() > 1 {
		split := ( elves.Len() / 2 )

		if cursor == nil {
			idx = 0
			cursor = elves.Front()
		}

		for idx != split {
			if idx < split {
				cursor = cursor.Next()
				idx++
			} else {
				cursor = cursor.Prev()
				idx--
			}
		}

		firstElf := elf(elves.Remove( elves.Front() ).(elf))
		carryOnFrom := cursor.Prev()
		idx-=2;

		middleElf := elf(elves.Remove( cursor ).(elf))

		firstElf.presents += middleElf.presents

		elves.PushBack( firstElf )
		cursor = carryOnFrom
	}

	return elf( elves.Front().Value.(elf)).id
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
