package main

import(
    "fmt"
	"os"
	"regexp"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {

	parse := regexp.MustCompile(`Disc #(\d+) has (\d+) positions; at time=(\d+), it is at position (\d+).`)

	mods := []int{}
	at := []int{}
	posis := []int{}
	for _, line := range input {
		values := parse.FindStringSubmatch( line )
		if len( values ) > 0 {
			disc, err := strconv.Atoi( values[1] )
			if err != nil {
				panic( err )
			}

			size, err := strconv.Atoi( values[2] )
			if err != nil {
				panic( err )
			}
			mods = append( mods, size )

			time, err := strconv.Atoi( values[3] )
			if err != nil {
				panic( err )
			}
			at = append( at, time )

			pos, err := strconv.Atoi( values[4] )
			if err != nil {
				panic( err )
			}

			posis = append( posis, size - ((pos + disc) % size) )
		}
	}

	answer, err := adventOfCode.Crt( mods, posis )
	if err != nil {
		panic( err )
	}

	return answer
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part1( append( input, "Disc #7 has 11 positions; at time=0, it is at position 0." ) )

	fmt.Println( "answer 2: ", answer2 )
}
