package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

const (
	TRAP = '^'
	SAFE = '.'
)

func Part1( input []string ) int {
	row := []rune(input[0])
	rows,err := strconv.Atoi( input[1] )
	if err != nil {
		panic( err )
	}

	safeTiles := countSafeTiles( row )

	for i := 1; i<rows; i++ {
		newRow := []rune{}
		for j := 0; j< len( row ); j++ {
			newRow = append( newRow, isTrap( j, row ) )
		}
		safeTiles += countSafeTiles( newRow )
		row = newRow
	}

	return safeTiles
}

func isTrap( pos int, row []rune ) rune {
	l, c, r := SAFE, row[ pos ], SAFE
	if pos > 0 {
		l = row[ pos - 1]
	}
	if pos < len( row ) - 1 {
		r  = row[ pos + 1 ]
	}

	if ( l == TRAP && c == TRAP && r == SAFE ) ||
	   ( l == SAFE && c == TRAP && r == TRAP ) ||
	   ( l == SAFE && c == SAFE && r == TRAP ) ||
	   ( l == TRAP && c == SAFE && r == SAFE ) {

		return TRAP
	}

	return SAFE
}


func countSafeTiles( row []rune ) int {
	safeTiles := 0

	for i:=0; i<len(row); i++ {
		if row[i] == SAFE {
			safeTiles++
		}
	}
	return safeTiles
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part1( append( input[0:1], "400000" ) )

	fmt.Println( "answer 2: ", answer2 )
}
