package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) string {

	buffer := []rune(input[0])
	rlength, err := strconv.Atoi( input[1] )
	if err != nil {
		panic( err )
	}

	for len( buffer ) < rlength {
		buffer = append( buffer, '0' )
		for i := len( buffer ) - 2; i>=0; i-- {
			buffer = append( buffer, not( buffer[i] ) )
		}
	}

	buffer = buffer[0:rlength]

	return string( checksum( buffer ) )
}

func not( a rune ) rune {
	if a == '1' {
		return '0'
	}

	return '1'
}

func xor( a rune, b rune ) rune {
	if a == b {
		return '0'
	}

	return '1'
}

func checksum( buffer []rune ) []rune {
	checksum := buffer
	for ( len( checksum ) % 2) == 0 {
		newChecksum := []rune{}
		for i:=0; i < len(checksum); i += 2 {
			newChecksum = append( newChecksum, not(xor( checksum[i], checksum[i+1] ) ) )
		}
		checksum = newChecksum
	}

	return checksum
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part1( append( input[0:1], "35651584" ) )

	fmt.Println( "answer 2: ", answer2 )
}
