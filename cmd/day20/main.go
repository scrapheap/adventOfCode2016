package main

import(
    "fmt"
	"os"
	"strconv"
	"regexp"
	"sort"
	"adventOfCode/internal/adventOfCode"
)

type ipRange struct {
	start int
	end int
}

type ipRanges []ipRange

func (a ipRanges) Len() int {
	return len(a)
}

func (a ipRanges) Swap( i, j int ) {
	a[i], a[j] = a[j], a[i]
}

func (a ipRanges) Less( i, j int ) bool {
	return a[i].start < a[j].start
}

func Part1( input []string ) int {
	ranges :=[]ipRange{}

	parse := regexp.MustCompile(`(\d+)\s*-\s*(\d+)`)
	for _, line := range input {
		 parts := parse.FindStringSubmatch( line )
		 if len(parts) > 0{
			start, err := strconv.Atoi( parts[1] )
			if err != nil {
				panic( err )
			}

			end, err := strconv.Atoi( parts[2] )
			if err != nil {
				panic( err )
			}

			ranges = append( ranges, ipRange{ start: start, end: end } )
		 }
	}

	sort.Sort( ipRanges( ranges ) )
	ranges = mergeRanges( ranges )

	ip := 0;

	for _, r := range ranges {
		if ip < r.start {
			return ip
		}
		ip = r.end + 1
	}

	return ip
}

func mergeRanges( ranges []ipRange ) []ipRange {
	mergedRanges := []ipRange{};

	currentRange := ranges[0]
	for _, r := range ranges[1:] {
		if ( r.start > currentRange.end ) {
			mergedRanges = append( mergedRanges, currentRange )
			currentRange = r
		} else {
			currentRange.start, _ = adventOfCode.Min( currentRange.start, r.start )
			currentRange.end, _ = adventOfCode.Max( currentRange.end, r.end )
		}
	}

	mergedRanges = append( mergedRanges, currentRange )

	return mergedRanges
}

func Part2( input []string ) int {
	ranges :=[]ipRange{}

	parse := regexp.MustCompile(`(\d+)\s*-\s*(\d+)`)
	for _, line := range input {
		 parts := parse.FindStringSubmatch( line )
		 if len(parts) > 0{
			start, err := strconv.Atoi( parts[1] )
			if err != nil {
				panic( err )
			}

			end, err := strconv.Atoi( parts[2] )
			if err != nil {
				panic( err )
			}

			ranges = append( ranges, ipRange{ start: start, end: end } )
		 }
	}

	sort.Sort( ipRanges( ranges ) )
	ranges = mergeRanges( ranges )

	ipsAllowed := 4294967296;

	for _, r := range ranges {
		ipsAllowed -= ( ( r.end - r.start) + 1 )
	}

	return ipsAllowed
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
