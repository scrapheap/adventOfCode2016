package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
        { input: "R2, L3", want: 5 },
        { input: "R2, R2, R2", want: 2 },
        { input: "R2, R2, R2, R2", want: 0 },
        { input: "R5, L5, R5, R3", want: 12 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}


func TestPart2( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
        { input: "R8, R4, R4, R8", want: 4 },
        { input: "R2, R2, R2, R2", want: 0 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
