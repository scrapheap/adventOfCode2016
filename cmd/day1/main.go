package main

import(
    "fmt"
	"regexp"
	"strconv"
	"os"
	"adventOfCode/internal/adventOfCode"
)

type location [2]int
type direction int

const (
	NORTH direction = iota
	EAST
	SOUTH
	WEST
)

const (
	RIGHT direction = 1
	LEFT  direction = 3
)

func turn( facing direction, way string ) direction {
	inc := RIGHT
	if way == "L" {
		inc = LEFT
	}

	return ( ( facing + inc ) % 4 );
}

func walkForwards( pos location, facing direction, distance int ) location {
	switch facing {
		case NORTH:
			pos[0] += distance;
		case EAST:
			pos[1] += distance;
		case SOUTH:
			pos[0] -= distance;
		case WEST:
			pos[1] -= distance;
	}

	return pos;
}

func abs( i int ) int {
	if i < 0 {
		return -i
	}

	return i
}

func Part1( input string ) int {
	parse := regexp.MustCompile(`([RL])(\d+)`)

	pos := location{ 0, 0 }

	facing := NORTH;

    for _, move := range parse.FindAllStringSubmatch( input, -1 ) {
		facing = turn( facing, move[1] )
		distance, err := strconv.Atoi( move[2] );
		if err != nil {
			panic( err )
		}
		pos = walkForwards( pos, facing, distance )
    }


	return abs(pos[0]) + abs(pos[1]);
}

func Part2( input string ) int {
	parse := regexp.MustCompile(`([RL])(\d+)`)

	seen := make(map[location]bool)

	pos := location{ 0, 0 }
	seen[pos] = true

	facing := NORTH;

    for _, move := range parse.FindAllStringSubmatch( input, -1 ) {
		facing = turn( facing, move[1] )
		distance, err := strconv.Atoi( move[2] );
		if err != nil {
			panic( err )
		}

		for i := 0; i < distance; i++ {
			pos = walkForwards( pos, facing, 1 )
			if seen[pos] {
				return abs(pos[0]) + abs(pos[1]);
			}
			seen[pos] = true
		}
    }


	return abs(pos[0]) + abs(pos[1]);
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input[0] )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input[0] )

	fmt.Println( "answer 2: ", answer2 )
}
