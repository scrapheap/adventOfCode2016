package main

import(
    "fmt"
	"os"
	"regexp"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type instruction struct {
	op string
	operand1 string
	operand2 string
}

func Part1( input []string ) int {

	instr := []instruction{}

	parseInstruction := regexp.MustCompile(`(\w+)\s+(\w+)\s*(-?\w*)`)
	for _, line := range input {
		part := parseInstruction.FindStringSubmatch( line )
		if len( part ) > 0  {
			instr = append( instr, instruction{
				op: part[1],
				operand1: part[2],
				operand2: part[3],
			} )
		}
	}

	reg := map[string]int{}
	isReg := map[string]bool{
		"a": true,
		"b": true,
		"c": true,
		"d": true,
	}

	for i:=0; i< len( instr ); i++ {
		switch instr[i].op {
			case "cpy":
				if isReg[instr[i].operand1] {
					reg[instr[i].operand2] = reg[instr[i].operand1]
				} else {
					if val, err := strconv.Atoi( instr[i].operand1 ); err == nil {
						reg[instr[i].operand2] = val
					}
				}
			case "inc":
				reg[instr[i].operand1]++
			case "dec":
				reg[instr[i].operand1]--
			case "jnz":
				val := 0
				if isReg[instr[i].operand1] {
					val = reg[instr[i].operand1]
				} else {
					t, err := strconv.Atoi( instr[i].operand2 )
					if err != nil {
						panic( err )
					}
					val = t
				}
				if val != 0 {
					if offset, err := strconv.Atoi( instr[i].operand2 ); err == nil {
						i += (offset - 1)
					}
				}
		}
	}

	return reg["a"]
}

func Part2( input []string ) int {

	instr := []instruction{}

	parseInstruction := regexp.MustCompile(`(\w+)\s+(\w+)\s*(-?\w*)`)
	for _, line := range input {
		part := parseInstruction.FindStringSubmatch( line )
		if len( part ) > 0  {
			instr = append( instr, instruction{
				op: part[1],
				operand1: part[2],
				operand2: part[3],
			} )
		}
	}

	reg := map[string]int{ "c": 1 }
	isReg := map[string]bool{
		"a": true,
		"b": true,
		"c": true,
		"d": true,
	}

	for i:=0; i< len( instr ); i++ {
		switch instr[i].op {
			case "cpy":
				if isReg[instr[i].operand1] {
					reg[instr[i].operand2] = reg[instr[i].operand1]
				} else {
					if val, err := strconv.Atoi( instr[i].operand1 ); err == nil {
						reg[instr[i].operand2] = val
					}
				}
			case "inc":
				reg[instr[i].operand1]++
			case "dec":
				reg[instr[i].operand1]--
			case "jnz":
				val := 0
				if isReg[instr[i].operand1] {
					val = reg[instr[i].operand1]
				} else {
					t, err := strconv.Atoi( instr[i].operand2 )
					if err != nil {
						panic( err )
					}
					val = t
				}
				if val != 0 {
					if offset, err := strconv.Atoi( instr[i].operand2 ); err == nil {
						i += (offset - 1)
					}
				}
		}
	}

	return reg["a"]
}


func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
