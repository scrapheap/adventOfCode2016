package main

import(
    "fmt"
	"regexp"
	"os"
	"strconv"
	"sort"
	"adventOfCode/internal/adventOfCode"
)

type room struct {
	id string
	sector int
	checksum string
};

func Part1( input []string ) int {

	sectorTotal := 0

	for _, line := range input {
		entry, err := parseRoom( line )
		if err != nil {
			panic( err )
		}

		if validRoom( entry ) {
			sectorTotal += entry.sector
		}
	}

	return sectorTotal
}

func parseRoom( line string ) ( room, error ) {
	parse := regexp.MustCompile(`(.*)\-(\d+)\[(\w+)\]`)
	component := parse.FindStringSubmatch( line )

	var err error

	entry := room{
		id: component[1],
		checksum: component[3],
	}

	if entry.sector, err = strconv.Atoi(component[2]); err != nil {
		return entry, err
	}

	return entry, nil
}

func validRoom( entry room ) bool {
	return checksum( entry.id ) == entry.checksum
}

func checksum( id string ) string {
	seen := map[rune]int{}

	for _, c := range id {
		if ( c != '-' ) {
			seen[c]+=1
		}
	}

	var keys []rune;
	for k:= range seen {
		keys = append( keys, k )
	}

	sort.Slice( keys, func( i, j int) bool {
		a := keys[i]
		b := keys[j]

		if ( seen[a] == seen[b] ) {
			return a < b
		}

		return seen[a] > seen[b]
	} )

	checksum := string( keys[0] );
	for i := 1; i < 5; i++ {
		checksum = checksum + string( keys[i] );
	}

	return checksum;
}

func Part2( input []string ) int {
	sector := 0

	for _, line := range input {
		entry, err := parseRoom( line )
		if err != nil {
			panic( err )
		}

		if validRoom( entry ) && decrypt( entry.id, entry.sector ) == "northpole object storage" {
			sector = entry.sector
		}
	}

	return sector
}

func decrypt( id string, offset int ) string {
	plaintext := ""

	for _, c := range id {
		if ( c == '-' ) {
			plaintext = plaintext + " "
		} else {
			plaintext = plaintext + string( rune( ( ( ( int( c ) - 'a' ) + offset ) % 26 ) ) + 'a' )
		}
	}

	return plaintext
}


func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
