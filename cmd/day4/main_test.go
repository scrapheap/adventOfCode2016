package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{ "aaaaa-bbb-z-y-x-123[abxyz]" }, want: 123 },
        { input: []string{ "a-b-c-d-e-f-g-h-987[abcde]" }, want: 987 },
        { input: []string{
			"not-a-real-room-404[oarel]",
			"totally-real-room-200[decoy]",
		}, want: 404 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{
			"qzmt-zixmtkozy-ivhz-343[zimth]",
			"lmprfnmjc-mzhcar-qrmpyec-548[mcrpa]",
		}, want: 548 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
