package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

func expand( input string ) string {
	output := []rune{}
	source := []rune( input )

	for i := 0 ; i<len(source) ; i++ {
		if source[i] == '(' {
			start, end, times := expansion( source, i )
			for ; times > 0; times-- {
				output = append( output, source[start:end]... )
			}
			i = end - 1
		} else {
			output = append( output, source[i] )
		}
	}

	return string( output )
}

func expansion( source []rune, i int ) ( int, int, int ) {
	w := []rune{}
	t := []rune{}
	inWindow := true

	j := i + 1
	for ; j < len( source ) && source[j] != ')'; j++ {
		if source[j] == 'x' {
			inWindow = false
		} else {
			if inWindow {
				w = append( w, source[j] )
			} else {
				t = append( t, source[j] )
			}
		}
	}

	start := j + 1

	window, err := strconv.Atoi( string( w ) );
	if err != nil {
		panic( err )
	}

	times, err := strconv.Atoi( string( t ) );
	if err != nil {
		panic( err )
	}

	return start, start + window, times
}

func Part1( input []string ) int {
	return len( expand( input[0] ) )
}

func Part2( input []string ) int {
	source := []rune( input[0] )
	weight := []int{};


	for i:=0; i < len( source ) ; i++ {
		weight = append( weight, 1 )
	}

	total := 0

	for i:=0; i< len( source ) ; i++ {
		if source[i] == '(' {
			start, end, times := expansion( source, i )
			for j := i; j < end; j++ {
				weight[j] *= times
			}

			i = start - 1
		} else {
			total += weight[i]
		}
	}

	return total
}


func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
