package main

import (
	"testing"
)

func TestExpand( t * testing.T ) {
	 type test struct {
        input string
        want  string
    }

	tests := []test{
        { input: "ADVENT", want: "ADVENT" },
        { input: "A(1x5)BC", want: "ABBBBBC" },
        { input: "(3x3)XYZ", want: "XYZXYZXYZ" },
        { input: "A(2x2)BCD(2x2)EFG", want: "ABCBCDEFEFG" },
        { input: "(6x1)(1x3)A", want: "(1x3)A" },
        { input: "X(8x2)(3x3)ABCY", want: "X(3x3)ABC(3x3)ABCY" },
    }

	for _, testCase := range tests {
        got := expand( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{ "ADVENT" }, want: 6 },
        { input: []string{ "A(1x5)BC" }, want: 7 },
        { input: []string{ "(3x3)XYZ" }, want: 9 },
        { input: []string{ "A(2x2)BCD(2x2)EFG" }, want: 11 },
        { input: []string{ "(6x1)(1x3)A" }, want: 6 },
        { input: []string{ "X(8x2)(3x3)ABCY" }, want: 18 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{ "(3x3)XYZ" }, want: 9 },
        { input: []string{ "X(8x2)(3x3)ABCY" }, want: 20 },
        { input: []string{ "(27x12)(20x12)(13x14)(7x10)(1x12)A" }, want: 241920 },
        { input: []string{ "(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN" }, want: 445 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
