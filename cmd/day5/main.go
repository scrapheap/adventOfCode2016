package main

import(
    "fmt"
	"os"
	"crypto/md5"
	"adventOfCode/internal/adventOfCode"
)

type room struct {
	id string
	sector int
	checksum string
};

func Part1( input []string ) string {

	doorId := input[0]
	doorCode := ""
	searching := true

	for count :=0; searching; count++ {
		attempt := fmt.Sprintf( "%s%d", doorId, count )
		hash := fmt.Sprintf( "%x", md5.Sum( []byte( attempt ) ) );
		if hash[0:5] == "00000" {
			doorCode = doorCode + string( hash[5] )
			if len( doorCode ) == 8 {
				searching = false
			}
		}
	}

	return doorCode
}


func Part2( input []string ) string {

	doorId := input[0]
	doorCode := [8]byte {}
	searching := true
	found := 0

	for count :=0; searching; count++ {
		attempt := fmt.Sprintf( "%s%d", doorId, count )
		hash := fmt.Sprintf( "%x", md5.Sum( []byte( attempt ) ) );
		if hash[0:5] == "00000" && hash[5] >= '0' && hash[5] <= '7' && doorCode[ hash[5] - '0' ] == 0 {
			doorCode[ hash[5] - '0' ] = hash[6]
			if found == 7 {
				searching = false
			}
			found++
		}
	}

	return fmt.Sprintf( "%s", doorCode)
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
