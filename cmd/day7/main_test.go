package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{ "abba[mnop]qrst" }, want: 1 },
        { input: []string{ "abcd[bddb]xyyx" }, want: 0 },
        { input: []string{ "aaaa[qwer]tyui" }, want: 0 },
        { input: []string{ "ioxxoj[asdfgh]zxcvbn" }, want: 1 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{ "aba[bab]xyz" }, want: 1 },
        { input: []string{ "xyx[xyx]xyx" }, want: 0 },
        { input: []string{ "aaa[kek]eke" }, want: 1 },
        { input: []string{ "zazbz[bzb]cdb" }, want: 1 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
