package main

import(
    "fmt"
	"os"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {

	answer := 0;

	for _, line := range input {
		if supportsTLS( line ) {
			answer++
		}
	}

	return answer
}

func supportsTLS( ip string ) bool {
	window := ""
	inHypernet := false
	foundABBA := false

	for i:=0; i<len(ip); i++ {
		switch string( ip[i] ) {
			case "[":
				window = ""
				inHypernet = true

			case "]":
				window = ""
				inHypernet = false

			default:
				window = updateWindow( window, string( ip[i] ), 4 )

				if len(window) == 4 && isABBA( window ) {
						if inHypernet {
							return false
						} else {
							foundABBA = true
						}
					}
		}
	}

	return foundABBA
}

func isABBA( window string ) bool {
	return window[0] != window[1] && window[0] == window[3] && window[1] == window[2]
}

func updateWindow( window, char string, size int ) string {
	if window = window + char; len( window ) > size {
		return window[1:]
	} else {
		return window
	}
}

func Part2( input []string ) int {

	answer := 0;

	for _, line := range input {
		if supportsSSL( line ) {
			answer++
		}
	}

	return answer
}

func supportsSSL( ip string ) bool {
	window := ""
	inHypernet := false
	supernetABAs := []string{}
	hypernetBABs := []string{}

	for i:=0; i<len(ip); i++ {
		switch string( ip[i] ) {
			case "[":
				window = ""
				inHypernet = true

			case "]":
				window = ""
				inHypernet = false

			default:
				window = updateWindow( window, string( ip[i] ), 3 )

				if len(window) == 3 && isABA( window ) {
					if inHypernet {
						hypernetBABs = append( hypernetBABs, window )
					} else {
						supernetABAs = append( supernetABAs, window )
					}
				}
		}
	}

	for _, aba := range supernetABAs {
		for _, bab := range hypernetBABs {
			if aba[0] == bab[1] && aba[1] == bab[0] {
				return true
			}
		}
	}

	return false
}

func isABA( window string ) bool {
	return window[0] == window[2] && window[0] != window[1]
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
