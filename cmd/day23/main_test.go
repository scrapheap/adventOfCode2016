package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{
			"cpy 2 a",
			"tgl a",
			"tgl a",
			"tgl a",
			"cpy 1 a",
			"dec a",
			"dec a",
		}, want: 3 },
        { input: []string{
			"cpy 41 a",
			"inc a",
			"inc a",
			"dec a",
			"jnz a 2",
			"dec a",
		}, want: 42 },
        { input: []string{
			"cpy 41 a",
			"inc a",
			"inc a",
			"dec a",
			"jnz a -1",
			"dec a",
		}, want: -1 },
        { input: []string{
			"cpy 0 a",
			"inc a",
			"cpy a b",
			"inc b",
			"cpy b c",
			"inc c",
			"cpy c d",
			"inc d",
			"cpy d a",
		}, want: 4 },
        { input: []string{
			"cpy 8 a",
			"dec a",
			"cpy a b",
			"dec b",
			"cpy b c",
			"dec c",
			"cpy c d",
			"dec d",
			"cpy d a",
		}, want: 4 },
        { input: []string{
			"cpy 10 b",
			"cpy 10 a",
			"dec a",
			"dec b",
			"jnz b -2",
		}, want: 0 },
        { input: []string{
			"cpy a b",
			"cpy 0 c",
			"inc c",
			"dec b",
			"jnz b -2",
			"cpy c a",
		}, want: 7 },
        { input: []string{
			"cpy -16 a",
		}, want: -16 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{
			"cpy 2 b",
			"cpy 3 c",
			"mul b c",
			"mul 5 c",
			"cpy 7 a",
			"add c a",
		}, want: 37 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
