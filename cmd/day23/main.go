package main

import(
    "fmt"
	"os"
	"regexp"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type instruction struct {
	op string
	operand1 string
	operand2 string
}

func Part1( input []string ) int {

	instr := []instruction{}

	parseInstruction := regexp.MustCompile(`(\w+)\s+(-?\w+)\s*(-?\w*)`)
	for _, line := range input {
		part := parseInstruction.FindStringSubmatch( line )
		if len( part ) > 0  {
			instr = append( instr, instruction{
				op: part[1],
				operand1: part[2],
				operand2: part[3],
			} )
		}
	}

	reg := map[string]int{}
	reg["a"] = 7

	isReg := map[string]bool{
		"a": true,
		"b": true,
		"c": true,
		"d": true,
	}

	swaps := map[string]string {
		"inc": "dec",
		"dec": "inc",
		"tgl": "inc",
		"jnz": "cpy",
		"cpy": "jnz",
	}

	fetch := func( operand string ) int {
		if isReg[operand] {
			return reg[operand]
		} else {
			if val, err := strconv.Atoi( operand ); err == nil {
				return val
			}
		}

		return 0
	}

	for i:=0; i< len( instr ); i++ {
		switch instr[i].op {
			case "cpy":
				if isReg[ instr[i].operand2 ] {
					reg[instr[i].operand2] = fetch( instr[i].operand1 )
				}
			case "inc":
				if isReg[ instr[i].operand1 ] {
					reg[instr[i].operand1]++
				}
			case "dec":
				if isReg[ instr[i].operand1 ] {
					reg[instr[i].operand1]--
				}
			case "jnz":
				if fetch( instr[i].operand1 ) != 0 {
					i += (fetch( instr[i].operand2 ) - 1)
				}
			case "tgl":
				offset := fetch( instr[i].operand1 )
				if i + offset >= 0 && i + offset < len( instr ) {
					instr[ i + offset ].op = swaps[ instr[ i + offset ].op ]
				}

		}
	}

	return reg["a"]
}

func Part2( input []string ) int {

	instr := []instruction{}

	parseInstruction := regexp.MustCompile(`(\w+)\s*(-?\w*)\s*(-?\w*)`)
	for _, line := range input {
		part := parseInstruction.FindStringSubmatch( line )
		if len( part ) > 0  {
			instr = append( instr, instruction{
				op: part[1],
				operand1: part[2],
				operand2: part[3],
			} )
		}
	}

	reg := map[string]int{}
	reg["a"] = 12

	isReg := map[string]bool{
		"a": true,
		"b": true,
		"c": true,
		"d": true,
	}

	swaps := map[string]string {
		"inc": "dec",
		"dec": "inc",
		"tgl": "inc",
		"jnz": "cpy",
		"cpy": "jnz",
	}

	fetch := func( operand string ) int {
		if isReg[operand] {
			return reg[operand]
		} else {
			if val, err := strconv.Atoi( operand ); err == nil {
				return val
			}
		}

		return 0
	}

	for i:=0; i< len( instr ); i++ {
		switch instr[i].op {
			case "mul":
				if isReg[ instr[i].operand2 ] {
					reg[instr[i].operand2] = fetch( instr[i].operand1 ) * reg[instr[i].operand2]
				}
			case "add":
				if isReg[ instr[i].operand2 ] {
					reg[instr[i].operand2] = fetch( instr[i].operand1 ) + reg[instr[i].operand2]
				}
			case "cpy":
				if isReg[ instr[i].operand2 ] {
					reg[instr[i].operand2] = fetch( instr[i].operand1 )
				}
			case "inc":
				if isReg[ instr[i].operand1 ] {
					reg[instr[i].operand1]++
				}
			case "dec":
				if isReg[ instr[i].operand1 ] {
					reg[instr[i].operand1]--
				}
			case "jnz":
				if fetch( instr[i].operand1 ) != 0 {
					i += (fetch( instr[i].operand2 ) - 1)
				}
			case "tgl":
				offset := fetch( instr[i].operand1 )
				if i + offset >= 0 && i + offset < len( instr ) {
					instr[ i + offset ].op = swaps[ instr[ i + offset ].op ]
				}
		}
	}

	return reg["a"]
}


func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	mulFix := []string{
		"cpy b a",
		"mul d a",
		"cpy 0 c",
		"cpy 0 d",
		"nop",
		"nop",
		"nop",
	}

	for i:=0; i< len(mulFix); i++ {
		input[i + 3] = mulFix[i]
	}

	mulFix = []string{
		"add d c",
		"cpy 0 d",
		"nop",
	}

	for i:=0; i< len(mulFix); i++ {
		input[i + 13] = mulFix[i]
	}

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
