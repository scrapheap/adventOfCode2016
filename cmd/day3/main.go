package main

import(
    "fmt"
	"regexp"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type triangle [3]int;

func Part1( input []string ) int {

	valid := 0

	for _, line := range input {
		t, err := parseTriangle( line )
		if err != nil {
			panic( err )
		}

		if validTriangle( t ) {

			valid++
		}
	}

	return valid;
}

func parseTriangle( line string ) ( triangle, error ) {
	parse := regexp.MustCompile(`(\d+)\s+(\d+)\s+(\d+)`)
	length := parse.FindStringSubmatch( line )

	var t triangle
	var err error

	for i:= 0; i<3; i++ {
		t[i], err = strconv.Atoi(length[i + 1])

		if err != nil {
			return t, err
		}
	}

	return t, nil
}

func validTriangle( t triangle ) bool {
	return ( t[0] + t[1] > t[2] ) && ( t[1] + t[2] > t[0] ) && ( t[2] + t[0] > t[1] )
}

func Part2( input []string ) int {

	valid := 0

	for i := 0; i < len(input); i+=3 {
		var source [3]triangle;
		var err error
		for j := 0; j < 3; j++ {
			source[j], err = parseTriangle( input[ i + j ] );
			if err != nil {
				panic( err )
			}
		}

		for j := 0; j < 3; j++ {
			t := triangle{ source[0][j], source[1][j], source[2][j] }
			if validTriangle( t ) {
				valid++;
			}
		}
	}

	return valid;
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
