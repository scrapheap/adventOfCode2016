package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{ "5 10 25" }, want: 0 },
        { input: []string{ "15 15 25" }, want: 1 },
        { input: []string{ "5 10 25", "15 15 25" }, want: 1 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }
	tests := []test{
        { input: []string{
			"101 301 501",
			"102 302 502",
			"103 303 503",
			"201 401   5",
			"202 402  10",
			"203 403  25",
		}, want: 5 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
