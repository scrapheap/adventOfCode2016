package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{
			"The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.",
			"The second floor contains a hydrogen generator.",
			"The third floor contains a lithium generator.",
			"The fourth floor contains nothing relevant.",
		}, want: 11 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
