package main

import(
    "fmt"
	"os"
	"regexp"
	"adventOfCode/internal/adventOfCode"
)


func Part1( input []string ) int {

	floorNums := map[string]int{
		"first": 0,
		"second": 1,
		"third": 2,
		"fourth": 3,
	}

	parseFloor := regexp.MustCompile(`The (first|second|third|fourth) floor contains`)
	parseComponents := regexp.MustCompile(`(generator|microchip)`)

	levels :=[4]int{0,0,0,0}
	for _, line := range input {
		part := parseFloor.FindStringSubmatch( line )
		floor := -1

		if len( part ) > 0  {
			floor = floorNums[ part[1] ]
		}

		components := parseComponents.FindAllStringSubmatch( line, -1 )
		levels[floor] += len( components )
	}

	total := ( 2 * ( levels[0] ) - 3 )+
		( 2 * ( levels[0] + levels[1] ) -3 ) +
		( 2 * ( levels[0] + levels[1] + levels[2] ) - 3 )

	return total;
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	input = append( input, "The first floor contains An elerium generator, An elerium-compatible microchip, A dilithium generator and a A dilithium-compatible microchip" )
	answer2 := Part1( input )

	fmt.Println( "answer 2: ", answer2 )
}
