package main

import(
    "fmt"
	"os"
	"regexp"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type instruction struct {
	op string
	x int
	y int
}

func Part1( input []string ) int {
	rows := 6
	cols := 50

	grid := [6][50]bool{}

	if ( input[0] == "testing" ) {
		input = input[1:]
		rows = 3
		cols = 7
	}

	for _, line := range input {
		command, err := parseCommand( line )
		if err != nil {
			panic( err )
		}

		switch command.op {
			case "rect":
				for row:=0; row<command.y; row++ {
					for col:=0; col<command.x; col++ {
						grid[row][col] = true
					}
				}
			case "rotate column":
				for i:=0; i<command.y; i++ {
					col := []bool{}
					for row :=0; row < rows; row++ {
						col = append( col, grid[row][command.x] )
					}

					for row :=0; row < rows - 1; row++ {
						grid[row + 1][command.x] =  col[row]
					}

					grid[0][command.x] = col[rows - 1]
				}

			case "rotate row":
				for i:=0; i<command.y; i++ {
					row := []bool{}
					for col :=0; col < cols; col++ {
						row = append( row, grid[command.x][col] )
					}

					for col :=0; col < cols - 1; col++ {
						grid[command.x][col + 1] =  row[col]
					}

					grid[command.x][0] = row[cols - 1]
				}
		}
	}

	lit := 0
	for row :=  0; row < rows; row++ {
		for col := 0; col < cols; col++ {
			if grid[row][col] {
				lit++
			}
		}
	}

	return lit
}

func parseCommand( line string ) ( instruction, error ) {
	parse := regexp.MustCompile(`(rect|rotate column|rotate row)\D+(\d+)\D+(\d+)`)
	part := parse.FindStringSubmatch( line )

	var instr instruction
	var err error

	instr.op = part[1];

	instr.x, err = strconv.Atoi(part[2])
	if err != nil {
		return instr, err
	}

	instr.y, err = strconv.Atoi(part[3])
	if err != nil {
		return instr, err
	}

	return instr, nil
}

func Part2( input []string ) string {
	rows := 6
	cols := 50

	grid := [6][50]bool{}

	if ( input[0] == "testing" ) {
		input = input[1:]
		rows = 3
		cols = 7
	}

	for _, line := range input {
		command, err := parseCommand( line )
		if err != nil {
			panic( err )
		}

		switch command.op {
			case "rect":
				for row:=0; row<command.y; row++ {
					for col:=0; col<command.x; col++ {
						grid[row][col] = true
					}
				}
			case "rotate column":
				for i:=0; i<command.y; i++ {
					col := []bool{}
					for row :=0; row < rows; row++ {
						col = append( col, grid[row][command.x] )
					}

					for row :=0; row < rows - 1; row++ {
						grid[row + 1][command.x] =  col[row]
					}

					grid[0][command.x] = col[rows - 1]
				}

			case "rotate row":
				for i:=0; i<command.y; i++ {
					row := []bool{}
					for col :=0; col < cols; col++ {
						row = append( row, grid[command.x][col] )
					}

					for col :=0; col < cols - 1; col++ {
						grid[command.x][col + 1] =  row[col]
					}

					grid[command.x][0] = row[cols - 1]
				}
		}

	}

	screen := "";
	for row:=0; row<rows; row++ {
		screen = screen + "\n"
		for col:=0; col<cols; col++ {
			if grid[row][col] {
				screen = screen + "#"
			} else {
				screen = screen + "."
			}
		}
	}

	return screen
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
