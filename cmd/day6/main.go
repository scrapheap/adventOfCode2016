package main

import(
    "fmt"
	"os"
	"adventOfCode/internal/adventOfCode"
)

type room struct {
	id string
	sector int
	checksum string
};

func Part1( input []string ) string {

	answer := "";

	for i := 0; i < len(input[0]); i++ {
		count := map[string]int {}

		for _, line := range input {
			count[string(line[i])]++
		}

		answer = answer + mostCommon( count )
	}

	return answer
}

func mostCommon( count map[string]int ) string {
	max := ""

	for k, v := range count {
		if ( v > count[max] ) {
			max = k
		}
	}

	return max
}

func Part2( input []string ) string {

	answer := "";

	for i := 0; i < len(input[0]); i++ {
		count := map[string]int {}

		for _, line := range input {
			count[string(line[i])]++
		}

		answer = answer + leastCommon( count )
	}

	return answer
}

func leastCommon( count map[string]int ) string {
	min := mostCommon( count )

	for k, v := range count {
		if ( v < count[min] ) {
			min = k
		}
	}

	return min
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
