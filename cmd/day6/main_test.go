package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  string
    }

	tests := []test{
        { input: []string{
			"eedadn",
			"drvtee",
			"eandsr",
			"raavrd",
			"atevrs",
			"tsrnev",
			"sdttsa",
			"rasrtv",
			"nssdts",
			"ntnada",
			"svetve",
			"tesnvt",
			"vntsnd",
			"vrdear",
			"dvrsen",
			"enarar",
		}, want: "easter" },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}


func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  string
    }

	tests := []test{
        { input: []string{
			"eedadn",
			"drvtee",
			"eandsr",
			"raavrd",
			"atevrs",
			"tsrnev",
			"sdttsa",
			"rasrtv",
			"nssdts",
			"ntnada",
			"svetve",
			"tesnvt",
			"vntsnd",
			"vrdear",
			"dvrsen",
			"enarar",
		}, want: "advent" },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
