package main

import(
    "fmt"
	"os"
	"adventOfCode/internal/adventOfCode"
)

type loc struct {
	row int
	col int
}

const (
	WALL = '#';
	FREE = '.';
)

func Part1( input []string ) int {
	grid := [][]rune{}

	for _, line := range input {
		grid = append( grid, []rune(line) )
	}

	points := map[int]loc{}

	for row := 0; row < len( grid ); row++ {
		for col := 0; col < len( grid[row] ); col++ {
			if grid[row][col] != WALL && grid[row][col] != FREE {
				points[int( grid[row][col] - '0' )] = loc{ row: row, col: col }
			}
		}
	}

	distances := map[int]map[int]int{}
	destinations := []int{}

	for idx, pos := range points {
		distances[idx] = findDistancesFrom( pos, grid )
		if idx != 0 {
			destinations = append( destinations, idx )
		}
	}

	possibleRoutes := generateRoutes( destinations )

	min := len(grid) * len(grid[0]);

	for _, route := range possibleRoutes {
		length := routeLength( append( []int{0}, route... ), distances )
		if length < min {
			min = length
		}
	}

	return min
}

type stepCount struct {
	pos loc
	steps int
}

func findDistancesFrom( pos loc, grid [][]rune ) map[int]int {
	neighbours := []stepCount{ {pos: pos, steps: 0 } };

	distanceTo := map[int]int{}
	seen := map[loc]bool{ pos: true }
	for len(neighbours) > 0 {
		current := neighbours[0]
		neighbours = neighbours[1:]

		if grid[current.pos.row][current.pos.col] != WALL && grid[current.pos.row][current.pos.col] != FREE {
			distanceTo[int( grid[current.pos.row][current.pos.col] - '0' )] = current.steps
		}

		if current.pos.row > 0 {
			north := loc{ row : current.pos.row - 1, col: current.pos.col }
			if grid[north.row][north.col] != WALL && !seen[north] {
				seen[north] = true
				neighbours = append( neighbours, stepCount{ pos: north, steps: current.steps + 1 } )
			}
		}
		if current.pos.row < len(grid) {
			south := loc{ row : current.pos.row + 1, col: current.pos.col }
			if grid[south.row][south.col] != WALL && !seen[south] {
				seen[south] = true
				neighbours = append( neighbours, stepCount{ pos: south, steps: current.steps + 1 } )
			}
		}
		if current.pos.col > 0 {
			west := loc{ row : current.pos.row, col: current.pos.col - 1 }
			if grid[west.row][west.col] != WALL && !seen[west] {
				seen[west] = true
				neighbours = append( neighbours, stepCount{ pos: west, steps: current.steps + 1 } )
			}
		}
		if current.pos.col < len(grid[0]) {
			east := loc{ row : current.pos.row, col: current.pos.col + 1 }
			if grid[east.row][east.col] != WALL && !seen[east] {
				seen[east] = true
				neighbours = append( neighbours, stepCount{ pos: east, steps: current.steps + 1 } )
			}
		}
	}

	return distanceTo
}

func generateRoutes( source []int ) [][]int {
    var helper func([]int, int)
    res := [][]int{}

    helper = func(arr []int, n int){
        if n == 1{
            tmp := make([]int, len(arr))
            copy(tmp, arr)
            res = append(res, tmp)
        } else {
            for i := 0; i < n; i++{
                helper(arr, n - 1)
                if n % 2 == 1{
                    tmp := arr[i]
                    arr[i] = arr[n - 1]
                    arr[n - 1] = tmp
                } else {
                    tmp := arr[0]
                    arr[0] = arr[n - 1]
                    arr[n - 1] = tmp
                }
            }
        }
    }
    helper(source, len(source))
    return res
}

func routeLength( route []int, distances map[int]map[int]int ) int {
	length := 0;

	for i:= 1; i<len(route); i++ {
		length += distances[route[i-1]][route[i]]
	}

	return length
}

func Part2( input []string ) int {
	grid := [][]rune{}

	for _, line := range input {
		grid = append( grid, []rune(line) )
	}

	points := map[int]loc{}

	for row := 0; row < len( grid ); row++ {
		for col := 0; col < len( grid[row] ); col++ {
			if grid[row][col] != WALL && grid[row][col] != FREE {
				points[int( grid[row][col] - '0' )] = loc{ row: row, col: col }
			}
		}
	}

	distances := map[int]map[int]int{}
	destinations := []int{}

	for idx, pos := range points {
		distances[idx] = findDistancesFrom( pos, grid )
		if idx != 0 {
			destinations = append( destinations, idx )
		}
	}

	possibleRoutes := generateRoutes( destinations )

	min := len(grid) * len(grid[0]);

	for _, route := range possibleRoutes {
		length := routeLength( append( append( []int{0}, route... ), 0), distances )
		if length < min {
			min = length
		}
	}

	return min
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
