package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{
			"###########",
			"#0.1.....2#",
			"#.#######.#",
			"#4.......3#",
			"###########",
		}, want: 14 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{
			"###########",
			"#0.1.....2#",
			"#.#######.#",
			"#4.......3#",
			"###########",
		}, want: 20 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
