package main

import(
    "fmt"
	"os"
	"regexp"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type bot struct {
	low string
	high string
	holding int
}

func Part1( input []string ) string {
	lookingFor := []int{17, 61}

	if input[0] == "testing" {
		lookingFor[0] = 2
		lookingFor[1] = 5
		input = input[1:]
	}

	bots := map[string]bot{}
	holding := map[string]int{}
	actions := []string{}

	parseBots := regexp.MustCompile(`(bot \d+) gives low to ((?:bot|output) \d+) and high to ((?:bot|output) \d+)`)
	for _, line := range input {
		part := parseBots.FindStringSubmatch( line )
		if len( part ) > 0  {
			bots[ part[1] ] = bot{
				low: part[2],
				high: part[3],
			}
			holding[ part[1] ] = -1
		} else {
			actions = append( actions, line )
		}
	}


	parseInputs := regexp.MustCompile(`value (\d+) goes to ((?:bot|output) \d+)`)
	for len(actions) > 0 {
		action := actions[0]
		if len(actions) > 0 {
			actions = actions[1:]
		}

		part := parseInputs.FindStringSubmatch( action )
		if len( part ) > 0  {
			chip, err := strconv.Atoi( part[1] )
			if err != nil {
				panic( err )
			}

			if holding[ part[2] ] != -1 {
				min, err := adventOfCode.Min( holding[ part[2] ], chip )
				if err != nil {
					panic( err )
				}
				max, err := adventOfCode.Max( holding[ part[2] ], chip )
				if err != nil {
					panic( err )
				}

				if min == lookingFor[0] && max == lookingFor[1] {
					return part[2];
				}

				actions = append(
					actions,
					fmt.Sprintf( "value %d goes to %s", min, bots[ part[2] ].low ),
					fmt.Sprintf( "value %d goes to %s", max, bots[ part[2] ].high ),
				)

				holding[ part[2] ] = -1

			} else {
				holding[ part[2] ] = chip
			}
		}
	}

	return ""
}

func Part2( input []string ) int {
	bots := map[string]bot{}
	holding := map[string]int{}
	actions := []string{}

	parseBots := regexp.MustCompile(`(bot \d+) gives low to ((?:bot|output) \d+) and high to ((?:bot|output) \d+)`)
	for _, line := range input {
		part := parseBots.FindStringSubmatch( line )
		if len( part ) > 0  {
			bots[ part[1] ] = bot{
				low: part[2],
				high: part[3],
			}
		} else {
			actions = append( actions, line )
		}
	}


	parseInputs := regexp.MustCompile(`value (\d+) goes to ((?:bot|output) \d+)`)
	for len(actions) > 0 {
		action := actions[0]
		if len(actions) > 0 {
			actions = actions[1:]
		}

		part := parseInputs.FindStringSubmatch( action )
		if len( part ) > 0  {
			chip, err := strconv.Atoi( part[1] )
			if err != nil {
				panic( err )
			}

			if _, ok := holding[ part[2] ]; ok {
				min, err := adventOfCode.Min( holding[ part[2] ], chip )
				if err != nil {
					panic( err )
				}
				max, err := adventOfCode.Max( holding[ part[2] ], chip )
				if err != nil {
					panic( err )
				}

				actions = append(
					actions,
					fmt.Sprintf( "value %d goes to %s", min, bots[ part[2] ].low ),
					fmt.Sprintf( "value %d goes to %s", max, bots[ part[2] ].high ),
				)
				delete( holding, part[2] )

			} else {
				holding[ part[2] ] = chip
			}
		}
	}

	return holding["output 0"] * holding["output 1"] * holding["output 2"]
}


func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
