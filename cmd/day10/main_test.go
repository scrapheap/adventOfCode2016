package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  string
    }

	tests := []test{
        { input: []string{
			"testing",
			"value 5 goes to bot 2",
			"bot 2 gives low to bot 1 and high to bot 0",
			"value 3 goes to bot 1",
			"bot 1 gives low to output 1 and high to bot 0",
			"bot 0 gives low to output 2 and high to output 0",
			"value 2 goes to bot 2",
		}, want: "bot 2" },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{
			"value 5 goes to bot 2",
			"bot 2 gives low to bot 1 and high to bot 0",
			"value 3 goes to bot 1",
			"bot 1 gives low to output 1 and high to bot 0",
			"bot 0 gives low to output 2 and high to output 0",
			"value 2 goes to bot 2",
		}, want: 30 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
