package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type loc struct {
	x int
	y int
}

type state struct {
	pos loc
	steps int
}


func Part1( input []string ) int {
	magic, err := strconv.Atoi(input[0])
	if err != nil {
		panic( err )
	}

	target := loc{ x: 31, y: 39 }

	if len(input) > 1 && input[1] == "testing" {
		target = loc{ x: 7, y: 4 }
	}

	grid := map[loc]int{}

	start := loc{ x: 1, y: 1};

	neighbours := []state{
		{ pos: start, steps: 0 },
	}

	for len( neighbours ) > 0 && grid[target] == 0 {
		st := neighbours[0];
		neighbours = neighbours[1:]

		n := north( st.pos )
		if ! isWall( n, magic ) && grid[n] == 0 {
			grid[n] = st.steps + 1
			neighbours = append( neighbours, state{ pos: n, steps: st.steps + 1 } )
		}

		e := east( st.pos )
		if ! isWall( e, magic ) && grid[e] == 0 {
			grid[e] = st.steps + 1
			neighbours = append( neighbours, state{ pos: e, steps: st.steps + 1 } )
		}

		s := south( st.pos )
		if ! isWall( s, magic ) && grid[s] == 0 {
			grid[s] = st.steps + 1
			neighbours = append( neighbours, state{ pos: s, steps: st.steps + 1 } )
		}

		w := west( st.pos )
		if ! isWall( w, magic ) && grid[w] == 0 {
			grid[w] = st.steps + 1
			neighbours = append( neighbours, state{ pos: w, steps: st.steps + 1 } )
		}
	}

	return grid[target]
}

func north( pos loc ) loc {
	return loc{ x: pos.x, y: pos.y - 1 }
}

func east( pos loc ) loc {
	return loc{ x: pos.x + 1, y: pos.y }
}

func south( pos loc ) loc {
	return loc{ x: pos.x, y: pos.y + 1 }
}

func west( pos loc ) loc {
	return loc{ x: pos.x - 1, y: pos.y }
}

func isWall( pos loc, magic int ) bool {
	x := pos.x
	y := pos.y
	return x < 0 || y < 0 || isOddParity( ( x*x + 3*x + 2*x*y + y + y*y ) + magic )
}

func isOddParity( v int ) bool {
	odd := false

	for i:=0; i<64; i++ {
		if ( v & ( 1<<i ) ) > 0 {
			odd = !odd
		}
	}

	return odd
}

func Part2( input []string ) int {
	magic, err := strconv.Atoi(input[0])
	if err != nil {
		panic( err )
	}

	within := 50

	if len(input) > 1 && input[1] == "testing" {
		within = 8
	}

	grid := map[loc]int{}

	start := loc{ x: 1, y: 1};

	neighbours := []state{
		{ pos: start, steps: 0 },
	}

	count := 0
	grid[start]=1

	for len( neighbours ) > 0 {
		st := neighbours[0];
		neighbours = neighbours[1:]

		if ( st.steps <= within ) {
			count++
			n := north( st.pos )
			if ! isWall( n, magic ) && grid[n] == 0 {
				grid[n] = st.steps + 1
				neighbours = append( neighbours, state{ pos: n, steps: st.steps + 1 } )
			}

			e := east( st.pos )
			if ! isWall( e, magic ) && grid[e] == 0 {
				grid[e] = st.steps + 1
				neighbours = append( neighbours, state{ pos: e, steps: st.steps + 1 } )
			}

			s := south( st.pos )
			if ! isWall( s, magic ) && grid[s] == 0 {
				grid[s] = st.steps + 1
				neighbours = append( neighbours, state{ pos: s, steps: st.steps + 1 } )
			}

			w := west( st.pos )
			if ! isWall( w, magic ) && grid[w] == 0 {
				grid[w] = st.steps + 1
				neighbours = append( neighbours, state{ pos: w, steps: st.steps + 1 } )
			}
		}
	}

	return count
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
