package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  string
    }

	tests := []test{
        { input: []string{
			"testing",
			"swap position 4 with position 0",
			"swap letter d with letter b",
			"reverse positions 0 through 4",
			"rotate left 1",
			"move position 1 to position 4",
			"move position 3 to position 0",
			"rotate based on position of letter b",
			"rotate based on position of letter d",
		}, want: "decab" },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  string
    }

	tests := []test{
        { input: []string{
			"decab",
			"swap position 4 with position 0",
			"swap letter d with letter b",
			"reverse positions 0 through 4",
			"rotate left 1",
			"move position 1 to position 4",
			"move position 3 to position 0",
			"rotate based on position of letter b",
			"rotate based on position of letter d",
		}, want: "abcde" },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
