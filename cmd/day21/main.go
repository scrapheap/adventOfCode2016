package main

import(
    "fmt"
	"os"
	"strconv"
	"regexp"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) string {
	hash := []rune("abcdefgh")

	if input[0] == "testing" {
		input = input[1:]
		hash = []rune("abcde")
	}

	parseOp := regexp.MustCompile(`^(swap|reverse|rotate|move)`)
	parseSwap := regexp.MustCompile(`(position|letter)\s+(\w+)\s+with\s+(?:position|letter)\s+(\w+)`)
	parseRotate := regexp.MustCompile(`(left|right|based)(?:\s+on\s+position\s+of\s+letter)?\s+(\w+)`)
	parseReverse := regexp.MustCompile(`positions\s+(\d+)\s+through\s+(\d+)`)
	parseMove := regexp.MustCompile(`position\s+(\d+)\s+to\s+position\s+(\d+)`)
	for _, line := range input {
		op := parseOp.FindStringSubmatch( line )
		if len(op) > 0{
			switch op[1] {
				case "swap":
					operands := parseSwap.FindStringSubmatch( line );
					i1 := 0;
					i2 := 0;
					if len(operands) > 0{
						if operands[1] == "letter" {
							i1 = findIndex( operands[2], hash )
							i2 = findIndex( operands[3], hash )
						} else {
							i1, _ = strconv.Atoi( operands[2] )
							i2, _ = strconv.Atoi( operands[3] )
						}
						hash[i1], hash[i2] = hash[i2], hash[i1]
					}
				case "rotate":
					operands := parseRotate.FindStringSubmatch( line );
					i1 := 0;
					if len(operands) > 0 {
						if operands[1] == "based" {
							i1 = findIndex( operands[2], hash )
							if i1 < 4 {
								i1++
							} else {
								i1 += 2
							}
						} else {
							i1, _ = strconv.Atoi( operands[2] )
						}

						if operands[1] == "left" {
							for i:=0; i< i1; i++ {
								hash = append( hash[1:], hash[0] )
							}
						} else {
							for i:=0; i< i1; i++ {
								hash = append( hash[len(hash)-1:], hash[0:len(hash)-1]... )
							}
						}
					}
				case "reverse":
					operands := parseReverse.FindStringSubmatch( line );
					if len(operands) > 0 {
						from, _ := strconv.Atoi( operands[1] )
						to, _ := strconv.Atoi( operands[2] )

						for i:=0; (i * 2) < to - from; i++ {
							hash[ from + i ], hash[ to - i ] = hash[ to - i ], hash[ from + i ]
						}
					}
				case "move":
					operands := parseMove.FindStringSubmatch( line );
					if len( operands ) > 0 {
						from, _ := strconv.Atoi( operands[1] )
						to, _ := strconv.Atoi( operands[2] )

						tmp := hash[from]
						hash = append( hash[0:from], hash[from+1:]... )
						hash = append( append( hash[0:to:to], tmp ), hash[to:]... )
					}
			}
		}
	}

	return string(hash)
}

func findIndex( letter string, hash []rune ) int {
	r := []rune( letter )

	for i:=0; i < len( hash ); i++ {
		if hash[i] == r[0] {
			return i
		}
	}

	return 0
}

func Part2( input []string ) string {
	targetHash := []rune(input[0])

	attempts := generateAttempts( []rune(input[0]) );

	input = input[1:]

	for _, attempt := range attempts {
		hash := hashPassword( attempt, input )

		if string( hash ) == string( targetHash ) {
			return string( attempt )
		}
	}

	return ""
}

func generateAttempts( source []rune ) [][]rune {
    var helper func([]rune, int)
    res := [][]rune{}

    helper = func(arr []rune, n int){
        if n == 1{
            tmp := make([]rune, len(arr))
            copy(tmp, arr)
            res = append(res, tmp)
        } else {
            for i := 0; i < n; i++{
                helper(arr, n - 1)
                if n % 2 == 1{
                    tmp := arr[i]
                    arr[i] = arr[n - 1]
                    arr[n - 1] = tmp
                } else {
                    tmp := arr[0]
                    arr[0] = arr[n - 1]
                    arr[n - 1] = tmp
                }
            }
        }
    }
    helper(source, len(source))
    return res
}

func uniq( attempt []rune ) bool {
	for i:=0; i< len( attempt ); i++ {
		for j:=i; j<len( attempt ); j++ {
			if (attempt[i] == attempt[j] ) {
				return false
			}
		}
	}
	return true
}

func hashPassword( hash []rune, rules []string ) []rune {
	parseOp := regexp.MustCompile(`^(swap|reverse|rotate|move)`)
	parseSwap := regexp.MustCompile(`(position|letter)\s+(\w+)\s+with\s+(?:position|letter)\s+(\w+)`)
	parseRotate := regexp.MustCompile(`(left|right|based)(?:\s+on\s+position\s+of\s+letter)?\s+(\w+)`)
	parseReverse := regexp.MustCompile(`positions\s+(\d+)\s+through\s+(\d+)`)
	parseMove := regexp.MustCompile(`position\s+(\d+)\s+to\s+position\s+(\d+)`)
	for _, line := range rules {
		op := parseOp.FindStringSubmatch( line )
		if len(op) > 0{
			switch op[1] {
				case "swap":
					operands := parseSwap.FindStringSubmatch( line );
					i1 := 0;
					i2 := 0;
					if len(operands) > 0{
						if operands[1] == "letter" {
							i1 = findIndex( operands[2], hash )
							i2 = findIndex( operands[3], hash )
						} else {
							i1, _ = strconv.Atoi( operands[2] )
							i2, _ = strconv.Atoi( operands[3] )
						}
						hash[i1], hash[i2] = hash[i2], hash[i1]
					}
				case "rotate":
					operands := parseRotate.FindStringSubmatch( line );
					i1 := 0;
					if len(operands) > 0 {
						if operands[1] == "based" {
							i1 = findIndex( operands[2], hash )
							if i1 < 4 {
								i1++
							} else {
								i1 += 2
							}
						} else {
							i1, _ = strconv.Atoi( operands[2] )
						}

						if operands[1] == "left" {
							for i:=0; i< i1; i++ {
								hash = append( hash[1:], hash[0] )
							}
						} else {
							for i:=0; i< i1; i++ {
								hash = append( hash[len(hash)-1:], hash[0:len(hash)-1]... )
							}
						}
					}
				case "reverse":
					operands := parseReverse.FindStringSubmatch( line );
					if len(operands) > 0 {
						from, _ := strconv.Atoi( operands[1] )
						to, _ := strconv.Atoi( operands[2] )

						for i:=0; (i * 2) < to - from; i++ {
							hash[ from + i ], hash[ to - i ] = hash[ to - i ], hash[ from + i ]
						}
					}
				case "move":
					operands := parseMove.FindStringSubmatch( line );
					if len( operands ) > 0 {
						from, _ := strconv.Atoi( operands[1] )
						to, _ := strconv.Atoi( operands[2] )

						tmp := hash[from]
						hash = append( hash[0:from], hash[from+1:]... )
						hash = append( append( hash[0:to:to], tmp ), hash[to:]... )
					}
			}
		}
	}

	return hash
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
