package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{ "abc" }, want: 22728 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	hash := stretchHash("577571be4de9dcce85a041ba0410f29f")
	if hash != "a107ff634856bb300138cac6568c0f24" {
		t.Fatalf("expected: %v, got: %v", "a107ff634856bb300138cac6568c0f24", hash )
	}

	tests := []test{
        { input: []string{ "abc" }, want: 22551 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
