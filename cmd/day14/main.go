package main

import(
    "fmt"
	"os"
	"crypto/md5"
	"regexp"
	"adventOfCode/internal/adventOfCode"
)

type possibleKey struct {
	char rune
	idx int
}

func Part1( input []string ) int {

	salt:= input[0]
	idx := 0

	keys := []possibleKey{}

	possibility := []possibleKey{}

	triple := regexp.MustCompile( `(000|111|222|333|444|555|666|777|888|999|aaa|bbb|ccc|ddd|eee|fff)` )
	penta := regexp.MustCompile( `(00000|11111|22222|33333|44444|55555|66666|77777|88888|99999|aaaaa|bbbbb|ccccc|ddddd|eeeee|fffff)` )

	for len(keys) < 64 {
		attempt := hash( salt, idx )

		series := penta.FindAllStringSubmatch( attempt, -1 )

		for i:=0; i< len( series ); i++ {
			c := []rune(series[i][1])[0]

			for j:=0; j< len( possibility ); j++ {
				if possibility[j].char == c  &&  possibility[j].idx + 1000 >= idx {
					keys = append( keys, possibility[j] )
					possibility = append( possibility[0:j], possibility[j+1:]...)
					j--
				}
			}
		}

		trip := triple.FindStringSubmatch( attempt )

		if len( trip ) > 0 {
			possibility = append( possibility, possibleKey{ char: []rune(trip[1])[0], idx: idx } )
		}

		idx++
	}

	return keys[63].idx
}


func hash( salt string, idx int ) string {
	return fmt.Sprintf( "%x", md5.Sum( []byte( fmt.Sprintf( "%s%d", salt, idx) ) ) )
}

func Part2( input []string ) int {

	salt:= input[0]
	idx := 0

	keys := []possibleKey{}

	possibility := []possibleKey{}

	triple := regexp.MustCompile( `(000|111|222|333|444|555|666|777|888|999|aaa|bbb|ccc|ddd|eee|fff)` )
	penta := regexp.MustCompile( `(00000|11111|22222|33333|44444|55555|66666|77777|88888|99999|aaaaa|bbbbb|ccccc|ddddd|eeeee|fffff)` )

	for len(keys) < 64 {
		attempt := stretchHash( hash( salt, idx ) )

		series := penta.FindAllStringSubmatch( attempt, -1 )

		for i:=0; i< len( series ); i++ {
			c := []rune(series[i][1])[0]

			for j:=0; j< len( possibility ); j++ {
				if possibility[j].char == c  &&  possibility[j].idx + 1000 >= idx {
					keys = append( keys, possibility[j] )
					possibility = append( possibility[0:j], possibility[j+1:]...)
					j--
				}
			}
		}

		trip := triple.FindStringSubmatch( attempt )

		if len( trip ) > 0 {
			possibility = append( possibility, possibleKey{ char: []rune(trip[1])[0], idx: idx } )
		}

		idx++
	}

	return keys[63].idx
}

func stretchHash( hash string ) string {
	for i:=0; i<2016; i++ {
		hash = fmt.Sprintf( "%x", md5.Sum( []byte( hash ) ) )
	}

	return hash
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
