package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{
			"out 0",
			"out 1",
			"jnz 1 -2",
		}, want: 0 },
        { input: []string{
			"out 0",
			"out 1",
			"jnz a -2",
		}, want: 1 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
