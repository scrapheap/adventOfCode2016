package main

import(
    "fmt"
	"os"
	"strconv"
	"regexp"
	"adventOfCode/internal/adventOfCode"
)

type stat struct {
	name string
	x int
	y int
	size int
	used int
	avail int
	perUsed int
}

func Part1( input []string ) int {
	parseNode := regexp.MustCompile(`^/dev/grid/([^ ]+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s+(\d+)%`)

	nodes := []stat{}

	for _, line := range input {
		df := parseNode.FindStringSubmatch( line )
		if len(df) > 0 {
			nodes = append( nodes, buildNode( df[1:] ) )
		}
	}

	viable := 0
	for i:=0; i<len(nodes); i++ {
		for j:=0; j<len(nodes); j++ {
			if i != j && nodes[i].used > 0 && nodes[i].used <= nodes[j].avail {
				viable++
			}
		}
	}

	return viable
}

func buildNode( df []string ) stat {
	stats := []int{}
	for i:=1; i<len(df); i++ {
		x, err := strconv.Atoi( df[i] );
		if err != nil {
			panic( err )
		}
		stats = append( stats, x )
	}

	return stat{
		name: df[0],
		size: stats[0],
		used: stats[1],
		avail: stats[2],
		perUsed: stats[3],
	}
}

func Part2( input []string ) string {
	parseNode := regexp.MustCompile(`^/dev/grid/([^ ]+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s+(\d+)%`)

	nodes := []stat{}

	maxX := 0;
	maxY := 0;
	for _, line := range input {
		df := parseNode.FindStringSubmatch( line )
		if len(df) > 0 {
			node := buildNode( df[1:] )
			node.x, node.y = parseNodeName( node.name )
			if node.x > maxX {
				maxX = node.x
			}
			if node.y > maxY {
				maxY = node.y
			}
			nodes = append(nodes, node)
		}
	}

	grid := [][]stat{}

	for y:=0; y<= maxY; y++ {
		row := []stat{}
		for x:=0; x<=maxX; x++ {
			row = append( row, stat{} )
		}
		grid = append( grid, row )
	}

	for _, node := range nodes {
		grid[node.y][node.x] = node
	}

	output := ""
	for _, row := range grid {
		line := "";
		for _, cell := range row {
			if cell.used == 0 {
				line += "_"
			} else if cell.used > 100 {
				line += "#"
			} else {
				line += "."
			}
		}
		output += line + "\n"
	}


	return output
}

func parseNodeName (name string) (int, int) {
	parseName := regexp.MustCompile(`^node-x(\d+)-y(\d+)`)
	coords := parseName.FindStringSubmatch( name )
	if len( coords ) > 0 {
		x, err:= strconv.Atoi( coords[1] )
		if err != nil {
			panic( err )
		}

		y, err:= strconv.Atoi( coords[2] )
		if err != nil {
			panic( err )
		}

		return x, y
	}

	return -1, -1
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}
