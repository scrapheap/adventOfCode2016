package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{
			"root@ebhq-gridcenter# df -h",
			"Filesystem              Size  Used  Avail  Use%",
			"/dev/grid/node-x0-y0     88T    0T    88T    0%",
			"/dev/grid/node-x0-y1     85T   20T    65T   85%",
			"/dev/grid/node-x0-y2     94T   73T    21T   77%",
			"/dev/grid/node-x0-y3     91T   72T    19T   79%",
			"/dev/grid/node-x1-y0     93T   71T    22T   76%",
			"/dev/grid/node-x1-y1     87T   65T    22T   74%",
			"/dev/grid/node-x1-y2     86T   69T    17T   80%",
			"/dev/grid/node-x1-y3     87T   67T    20T   77%",
			"/dev/grid/node-x2-y0     89T   71T    18T   79%",
			"/dev/grid/node-x2-y1     88T   72T    16T   81%",
			"/dev/grid/node-x2-y2     86T   73T    13T   84%",
			"/dev/grid/node-x2-y3     94T   68T    26T   72%",
			"/dev/grid/node-x3-y0     87T   67T    20T   77%",
			"/dev/grid/node-x3-y1     91T   69T    22T   75%",
			"/dev/grid/node-x3-y2     85T   69T    16T   81%",
			"/dev/grid/node-x3-y3     90T   72T    18T   80%",
		}, want: 23 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

// Part 2 is pretty printed and then worked out by hand
