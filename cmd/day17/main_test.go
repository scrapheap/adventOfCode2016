package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  string
    }

	tests := []test{
        { input: []string{ "hijkl" }, want: "" },
        { input: []string{ "ihgpwlah" }, want: "DDRRRD" },
        { input: []string{ "kglvqrro" }, want: "DDUDRLRRUDRD" },
        { input: []string{ "ulqzkmiv" }, want: "DRURDRUDDLLDLUURRDULRLDUUDDDRR" },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
        { input: []string{ "ihgpwlah" }, want: 370 },
        { input: []string{ "kglvqrro" }, want: 492 },
        { input: []string{ "ulqzkmiv" }, want: 830 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
