package main

import(
    "fmt"
	"os"
	"crypto/md5"
	"adventOfCode/internal/adventOfCode"
)

type loc struct {
	x int
	y int
}

type state struct {
	pos loc
	route string
}

const (
	NORTH   = 0
	EAST    = 3
	SOUTH   = 1
	WEST    = 2
)

func Part1( input []string ) string {
	code := input[0]

	states := []state{
		{
			pos: loc{ x: 0, y: 0 },
			route: "",
		},
	}

	target := loc{ x: 3, y: 3 }

	for len( states ) > 0 && len( states ) < 100 {
		attempt := states[0]
		states = states[1:]

		if ( attempt.pos == target ) {
			return attempt.route
		}

		n, e, s, w := doors( attempt.pos, code, attempt.route )

		if n {
			states = append( states, moveNorth( attempt ) )
		}
		if e {
			states = append( states, moveEast( attempt ) )
		}
		if s {
			states = append( states, moveSouth( attempt ) )
		}
		if w {
			states = append( states, moveWest( attempt ) )
		}
	}

	return ""
}

func doors( pos loc, code string, route string ) ( bool, bool, bool, bool ) {

	door := []rune( fmt.Sprintf( "%x", md5.Sum( []byte( code + route ) ) ) )[0:4]

	n := pos.x > 0 && unlocked( door[ NORTH ] )
	e := pos.y < 3 && unlocked( door[ EAST ] )
	s := pos.x < 3 && unlocked( door[ SOUTH ] )
	w := pos.y > 0 && unlocked( door[ WEST ] )

	return n, e, s, w
}

func unlocked( door rune ) bool {
	return door >= 'b' && door <= 'f'
}

func moveNorth( current state ) state {
	return state{ pos: loc{ x: current.pos.x - 1, y: current.pos.y }, route: current.route + "U"}
}

func moveEast( current state ) state {
	return state{ pos: loc{ x: current.pos.x, y: current.pos.y + 1 }, route: current.route + "R"}
}

func moveSouth( current state ) state {
	return state{ pos: loc{ x: current.pos.x + 1, y: current.pos.y }, route: current.route + "D"}
}

func moveWest( current state ) state {
	return state{ pos: loc{ x: current.pos.x, y: current.pos.y - 1 }, route: current.route + "L"}
}

func Part2( input []string ) int {
	code := input[0]

	max := 0;
	states := []state{
		{
			pos: loc{ x: 0, y: 0 },
			route: "",
		},
	}

	target := loc{ x: 3, y: 3 }

	for len( states ) > 0 {
		attempt := states[0]
		states = states[1:]

		if ( attempt.pos == target ) {
			if len( attempt.route ) > max {
				max = len( attempt.route )
			}
		} else {

			n, e, s, w := doors( attempt.pos, code, attempt.route )

			if n {
				states = append( states, moveNorth( attempt ) )
			}
			if e {
				states = append( states, moveEast( attempt ) )
			}
			if s {
				states = append( states, moveSouth( attempt ) )
			}
			if w {
				states = append( states, moveWest( attempt ) )
			}
		}
	}

	return max
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( append( input[0:1], "35651584" ) )

	fmt.Println( "answer 2: ", answer2 )
}
